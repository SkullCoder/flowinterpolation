from numba import *
from numba import cuda
from tqdm import tqdm
import numpy as np

import interpolation.utility as utils


@cuda.jit(device=True)
def toCoordGPU(frame, x, y):
    h = frame.shape[0]
    w = frame.shape[1]
    ix = (x - w / 2) / w
    iy = (h / 2 - y) / h
    return (ix, iy)


@cuda.jit(device=True)
def toImgGPU(frame, x, y):
    h = frame.shape[0]
    w = frame.shape[1]
    cx = (x + 0.5) * w
    cy = (-y + 0.5) * h
    return (cx, cy)


@cuda.jit(device=True)
def toPixGPU(frame, x, y):
    (ix, iy) = toImgGPU(frame, x, y)
    return (int(round(ix)), int(round(iy)))


@cuda.jit(device=True)
def insideGPU(frame, x, y):
    return ((0 <= y < frame.shape[0]) and (0 <= x < frame.shape[1]))


def toCoord(frame, x, y):
    h = frame.shape[0]
    w = frame.shape[1]
    ix = (x - w / 2) / w
    iy = (h / 2 - y) / h
    return (ix, iy)


def toImg(frame, x, y):
    h = frame.shape[0]
    w = frame.shape[1]
    cx = (x + 0.5) * w
    cy = (-y + 0.5) * h
    return (cx, cy)


def toPixel(frame, x, y):
    (ix, iy) = toImg(frame, x, y)
    return (int(round(ix)), int(round(iy)))


def apply(frame, x, y):
    center = toPixel(frame, x, y)
    pixels = [[center[0], center[1]], [0, 0], [0, 0], [0, 0]]
    if x > center[0]:
        # print("x:",x)
        # print("center[0]:",center[0])
        # print("f.shape[1]:",f.shape[1])
        # exit(0)
        pixels[1][0] = center[0] + 1
        pixels[2][0] = center[0]
        pixels[3][0] = center[0] + 1
    else:
        pixels[1][0] = center[0] - 1
        pixels[2][0] = center[0]
        pixels[3][0] = center[0] - 1
    if y > center[1]:
        # print("y:",y)
        # print("center[1]:",center[1])
        # print("f.shape[0]:",f.shape[0])
        # exit(0)
        pixels[1][1] = center[1]
        pixels[2][1] = center[1] + 1
        pixels[3][1] = center[1] + 1
    else:
        pixels[1][1] = center[1]
        pixels[2][1] = center[1] - 1
        pixels[3][1] = center[1] - 1
    return pixels


def changeByFlow(frame0, frame1, u,
                 row, col, t=0.5):
    h = frame0.shape[0]
    w = frame0.shape[1]
    (x, y) = toCoord(frame0, col, row)
    ux = u[0] / w
    uy = -u[1] / h
    xp0 = x - t * ux
    yp0 = y - t * uy
    xp1 = x + t * ux
    yp1 = y + t * uy
    (xi0, yi0) = toPixel(frame0, xp0, yp0)
    (xi1, yi1) = toPixel(frame1, xp1, yp1)
    if (inside(frame0, xi0, yi0) and
            inside(frame1, xi1, yi1)):
        i0 = frame0[yi0][xi0]
        i1 = frame1[yi1][xi1]
        return utils.dist(i0, i1)
    else:
        return 2


def applyFlow(flow, frame0, frame1, frame,
              row, col, t=0.5, forward=True):
    h = frame0.shape[0]
    w = frame0.shape[1]
    motion = flow[row][col] if forward else -1 * flow[row][col]
    ux = motion[0] / w
    uy = -motion[1] / h if forward else motion[1] / h
    (x, y) = toCoord(frame0, col, row)
    xp = x + t * ux
    yp = y + t * uy
    splats = apply(frame, xp, yp)
    for s in splats:
        if inside(frame, s[0], s[1]):
            old = frame[s[1]][s[0]]
            if np.isnan(old[0]) or np.isnan(old[1]):
                frame[s[1]][s[0]] = motion
            else:
                old = changeByFlow(frame0, frame1,
                                   old, s[1], s[0], t)
                new = changeByFlow(frame0, frame1,
                                   motion, s[1], s[0], t)
                if new < old:
                    frame[s[1]][s[0]] = motion


def combineMotion(forward, backward, frame0, frame1, t=0.5):
    h = frame0.shape[0]
    w = frame0.shape[1]

    if backward is None:
        frame = forward
    else:
        frame = np.zeros_like(forward)
        frame[:] = np.NAN
        for row in tqdm(range(h), desc="splatting forward"):
            for col in range(w):
                applyFlow(forward, frame0, frame1, frame,
                          row, col, t, forward=True)
        for row in tqdm(range(h), desc="splatting backward"):
            for col in range(w):
                applyFlow(backward, frame0, frame1, frame,
                          row, col, t, forward=False)
    fill(frame)
    fill(frame)
    deleteNan(frame)
    return frame


def deleteNan(frame):
    h = frame.shape[0]
    w = frame.shape[1]
    for r in range(h):
        for c in range(w):
            if np.isnan(frame[r][c][0]) or np.isnan(frame[r][c][1]):
                frame[r][c] = np.array([0.0, 0.0], dtype='float')


def fill(frame):
    h = frame.shape[0]
    w = frame.shape[1]
    indices = []
    for r in range(h):
        for c in range(w):
            indices.append((r, c))
    indices = notNan(frame, indices)
    indices.sort(key=lambda x: countNan(frame, x[0], x[1]))
    for i in tqdm(indices, desc="filling holes"):
        average_fill(frame, i)


def average_fill(f, indices):
    n = 0
    u = 0.0
    v = 0.0
    for i in range(-1, 2):
        for j in range(-1, 2):
            r = indices[0] + i
            c = indices[1] + j
            if inside(f, c, r):
                pixel = f[r][c]
                if not np.isnan(pixel[0]) and not np.isnan(pixel[1]):
                    n += 1
                    u += pixel[0]
                    v += pixel[1]
    if n != 0:
        averaged = np.array([u / n, v / n], dtype='float')
        f[indices[0]][indices[1]] = averaged


def countNan(f, r, c):
    nan = 0
    for i in range(-1, 2):
        for j in range(-1, 2):
            rp = r + i
            cp = c + j
            if inside(f, cp, rp):
                pixel = f[rp][cp]
                if np.isnan(pixel[0]) or np.isnan(pixel[1]):
                    nan += 1
    return nan


def notNan(frame, ind):
    nNan = []
    for i in ind:
        pixel = frame[i[0]][i[1]]
        if np.isnan(pixel[0]) or np.isnan(pixel[1]):
            nNan.append(i)
    return nNan


def inside(frame, x, y):
    return (0 <= y < frame.shape[0]) and (0 <= x < frame.shape[1])


def flowDist(m0, m1):
    assert (m0.shape == (2,))
    assert (m1.shape == (2,))
    m0h = np.array([m0[0], m0[1], 1])
    m1h = np.array([m1[0], m1[1], 1])
    top = m0h.dot(m1h)

    return 1 - (top / (np.linalg.norm(m0h) * np.linalg.norm(m1h)))

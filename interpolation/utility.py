import math
import shutil
import struct
import subprocess

import cv2
import numpy as np
from joblib import Parallel, delayed
from tqdm import tqdm


def load(path):
    capture = cv2.VideoCapture(path)
    frames = []
    frameCount = 0
    while capture.isOpened():
        valid, frame = capture.read()
        if valid:
            frames.append(frame)
        else:
            break
        frameCount += 1
    capture.release()
    return frames


def save(video, path, frameRate=30.0):
    dimension = video[0].shape
    print("Save to %s (%s)" %
          (path, str(dimension)))
    fourcc = cv2.VideoWriter_fourcc(*'XVID')
    writer = cv2.VideoWriter(path, fourcc, frameRate,
                          (dimension[1], dimension[0]))
    for frame in video:
        writer.write(frame)
    print("Saved.")
    writer.release()


def calcFlow(f1, f2):
    prev = cv2.cvtColor(f1, cv2.COLOR_BGR2GRAY)
    next = cv2.cvtColor(f2, cv2.COLOR_BGR2GRAY)
    uflow = np.zeros_like(prev)
    flow = cv2.calcOpticalFlowFarneback(prev,
                                        next,
                                        uflow,
                                        0.5,  # pyr scale
                                        3,  # levels
                                        25,  # winsize
                                        3,  # iterations
                                        7,  # poly_n
                                        1.5,  # ploy_sigma
                                        cv2.OPTFLOW_FARNEBACK_GAUSSIAN)
    return flow


# currently unused
def getFlow(frames):
    flows = []
    while len(frames) > 1:
        flows.append(calcFlow(frames[0], frames[1]))
        frames = frames[1:]
    return flows


def saveFramesNpy(fs, path):
    temp = np.stack(fs, axis=3)
    np.save(path, temp)


def dist(c1, c2):
    c1 = c1.astype('float')
    c2 = c2.astype('float')
    return math.sqrt((c1[0] - c2[0]) ** 2 + (c1[1] - c2[1]) ** 2
                     + (c1[2] - c2[2]) ** 2)


def saveFramesImg(frames, path):
    for i in range(len(frames)):
        file = path + "/frame" + str(i) + ".ppm"
        cv2.imwrite(file, frames[i])


def calc(input, output, ldofPath, i, forward=False):
    first = input + "/frame" + str(i) + ".ppm"
    second = input + "/frame" + str(i + 1) + ".ppm"
    if forward:
        subprocess.call([ldofPath, first, second])
    else:
        subprocess.call([ldofPath, second, first])

    out = "/frame" + str(i if forward else (i + 1)) + "LDOF"
    shutil.move(input + out + ".ppm", output + out + ".ppm")
    shutil.move(input + out + ".flo", output + out + ".flo")


def calcFlows(frameCount, input, output, ldofPath, forward=False, processes=1):
    Parallel(n_jobs=processes)(
        delayed(calc)(input, output, ldofPath, i, forward)
        for i in tqdm(range(frameCount - 1),
                      desc="calculating " + ("forward" if forward else "backward") + " flows")
    )


def loadFlow(frameCount, path, forward=False):
    offset = 1
    if forward:
        offset = 0
    flows = []
    for i in range(frameCount - 1):
        frame = path + "/frame" + str(i + offset) + "LDOF" + ".flo"
        flows.append(loadFrameFlow(frame))
    return flows


def loadFrameFlow(path):
    with open(path, 'rb') as f:
        pieh = struct.unpack('f', f.read(4))
        assert (pieh[0] == 202021.25)
        width = struct.unpack('i', f.read(4))[0]
        height = struct.unpack('i', f.read(4))[0]
        print("Load %s (%d x %d)" % (path, width, height))
        data = np.ndarray((height, width, 2))
        for r in range(height):
            for c in range(width):
                u = struct.unpack('f', f.read(4))[0]
                v = struct.unpack('f', f.read(4))[0]
                data[r][c] = np.array([u, v])
    return data

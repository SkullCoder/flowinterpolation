import cv2
from tqdm import tqdm

import interpolation.transfer as transfer
import interpolation.manipulate as manipulate
import interpolation.utility as utils


def interpolate(frames, forwards, backwards, t, i):
    flow = manipulate.combineMotion(forwards[i], backwards[i],
                                    frames[i], frames[i + 1], t)
    flow = cv2.GaussianBlur(flow, (11, 11), 10)
    interpolated = transfer.transfer(frames[i],
                                     frames[i + 1],
                                     forwards[i],
                                     backwards[i],
                                     flow,
                                     t)
    return ((frames[0], interpolated), flow)


def interpolateFrames(frames, forwards, backwards, t=0.5, processes=1):
    # commented to try non parallel gpu
    # res=Parallel(n_jobs=processes)(
    #     delayed(interpolate)(frames,forwards,backwards,t,i)
    #     for i in tqdm(range(len(frames)-1))
    # )
    res = []
    for i in tqdm(range(len(frames) - 1)):
        res.append(interpolate(frames, forwards, backwards, t, i))

    retImgs, flows = zip(*res)
    imgs = []
    for x, y in retImgs:
        imgs.append(x)
        imgs.append(y)

    utils.saveFramesNpy(flows, "./flows/imgs/flows.npy")

    return (imgs, flows)

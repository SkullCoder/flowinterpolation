import math

import numpy as np
import numba
from numba import *
from numba import cuda
from tqdm import tqdm

import interpolation.manipulate as manipulate


# import pycuda.autoinit
# import pycuda.driver as drv
# from pycuda.compiler import SourceModule


@cuda.jit
def maskedTransfer(f0, f1, dest, flow, mask, row, col, t=0.5):
    # for now, just default to using the value at f1:
    # h = dest.shape[0]
    # w = dest.shape[1]
    # u = flow[row][col]#direct split
    # ux = u[0] / w
    # uy = -u[1] / h
    # (x, y) = manipulate.toCoordGPU(flow, col, row)
    # xp0 = x - t * ux
    # yp0 = y - t * uy
    # xp1 = x + t * ux
    # yp1 = y + t * uy
    # (xi0, yi0) = manipulate.toPixGPU(flow, xp0, yp0)
    # (xi1, yi1) = manipulate.toPixGPU(flow, xp1, yp1)
    # if (manipulate.insideGPU(flow, xi0, yi0) and
    #         manipulate.insideGPU(flow, xi1, yi1)):
    # if  (((0 <= yi0 < h) and (0 <= xi0< w)) and
    #         ((0 <= yi1 < h) and (0 <= xi1 < w))) :
    #     # print(42)
    #     pass
    # f0vis = mask[row][col][0]
    # f1vis = mask[row][col][1]
    # if f0vis >= 1 and f1vis >= 1:
    #     for i in range(0,3):
    #         dest[row][col][i] = (f0[yi0][xi0][i]) / 2 + (f1[yi1][xi1][i]) / 2
    #     # dest[row][col] = (f0[yi0][xi0]) / 2 + (f1[yi1][xi1]) / 2
    # elif f1vis >= 1:
    #     for i in range(0,3):
    #         dest[row][col][i] = f1[yi1][xi1][i]
    #     # dest[row][col] = f1[yi1][xi1]
    # else:
    #     for i in range(0,3):
    #         dest[row][col][i] = f0[yi1][xi1][i]
    #     # dest[row][col] = f0[yi0][xi0]
    pass


# ("void(uint8)")
# ("void(array(uint8, 3d))")
# ("void(array(uint8, 3d),uint8,unit8,unit8)")
# @cuda.jit("void(uint8[:,:,:],uint8,unit8,unit8)")

@cuda.jit(void(int32, int32))
def f(x, y):
    a = x + y


# @cuda.jit(void(uint8,uint8,uint8,uint8,uint8))
# @ cuda.jit(void(uint8[:,:,:],uint8,uint8,uint8,uint8))
@cuda.jit("void(uint8[:,:,:],uint8,uint8,uint8,uint8)")
def setValue(f, row, col, i, val):
    f[row, col, i] = val
    # pass


# @vectorize(['void(uint8[:,:,:],uint8[:,:,:],float64[:,:,:],float64[:,:,:],float64[:,:,:],uint8[:,:,:],uint8[:,:,:],float64)'], target='cuda')
@cuda.jit(void(
    uint8[:, :, :],
    uint8[:, :, :],
    float64[:, :, :],
    float64[:, :, :],
    float64[:, :, :],
    uint8[:, :, :],
    uint8[:, :, :],
    float64
))
def colorTransGPU(f0, f1, forward, backward, interp, mask, newFrame, t=0.5):
    # h = interp.shape[0]
    # w = interp.shape[1]
    #
    # startX, startY = cuda.grid(2)
    # # startX = cuda.blockDim.x * cuda.blockIdx.x + cuda.threadIdx.x
    # # startY = cuda.blockDim.y * cuda.blockIdx.y + cuda.threadIdx.y
    # gridX = cuda.gridDim.x * cuda.blockDim.x;
    # gridY = cuda.gridDim.y * cuda.blockDim.y;

    # gdb_init()

    h = interp.shape[0]
    w = interp.shape[1]

    y, x = cuda.grid(2)
    # startX = cuda.blockDim.x * cuda.blockIdx.x + cuda.threadIdx.x
    # startY = cuda.blockDim.y * cuda.blockIdx.y + cuda.threadIdx.y
    gridX = cuda.gridDim.x * cuda.blockDim.x
    gridY = cuda.gridDim.y * cuda.blockDim.y
    # print(startX, w, gridX)

    if x > w or y > h:
        return

    # print(x,",",y)

    # maskedTransfer(f0, f1, newFrame, interp, mask, y, x, t)
    # def maskedTransfer(f0, f1, dest, flow, mask, row, col, t=0.5):
    row = np.uint8(y)
    col = np.uint8(x)
    flow = interp

    # red=newFrame[row,col,0]
    # green=newFrame[row,col,1]
    # blue=newFrame[row,col,2]

    red = green = blue = 0

    # for i in range(0,3):
    #     # newcol[i]=255-newFrame[row,col,i]
    #     newFrame[row, col, i] = 255-newFrame[row,col,i]

    h = newFrame.shape[0]
    w = newFrame.shape[1]
    u = flow[row][col]  # direct split
    ux = u[0] / w
    uy = -u[1] / h
    (x, y) = manipulate.toCoordGPU(flow, col, row)
    xp0 = x - t * ux
    yp0 = y - t * uy
    xp1 = x + t * ux
    yp1 = y + t * uy
    (xi0, yi0) = manipulate.toPixGPU(flow, xp0, yp0)
    (xi1, yi1) = manipulate.toPixGPU(flow, xp1, yp1)

    # if True:
    # if (manipulate.insideGPU(flow, xi0, yi0) and
    #         manipulate.insideGPU(flow, xi1, yi1)):
    # red = 42

    if (((0 <= yi0 < h) and (0 <= xi0 < w)) and
            ((0 <= yi1 < h) and (0 <= xi1 < w))):
        #     print(42)
        f0vis = mask[row][col][0]
        f1vis = mask[row][col][1]
        if f0vis >= 1 and f1vis >= 1:
            # red=42
            red = 13
            # if (manipulate.insideGPU(f0, xi0, yi0) and
            #     manipulate.insideGPU(f1, xi1, yi1)):
            #     # setValue_jit=cuda.jit("void(uint8[:,:,:],uint8,uint8,uint8,uint8)")(setValue)
            #     # setValue_jit(newFrame,row,col,0,42)
            #     # setValue(newFrame,row,col,0,42)
            #     red = 13#(f0[yi0][xi0][0]) / 2 + (f1[yi1][xi1][0]) / 2
            # else:
            #     red=42
            for i in range(0, 3):
                # newFrame[row,col,i]=42
                # newFrame[row, col, i] = 255-newFrame[row,col,i]
                x = i
                # pass
                # newFrame[row][col][i] = (f0[yi0][xi0][i]) / 2 + (f1[yi1][xi1][i]) / 2
        #     # dest[row][col] = (f0[yi0][xi0]) / 2 + (f1[yi1][xi1]) / 2
        elif f1vis >= 1:
            red = 42
            for i in range(0, 3):
                pass
        #         newFrame[row][col][i] = f1[yi1][xi1][i]
        #     # dest[row][col] = f1[yi1][xi1]
        else:
            red = 42
            for i in range(0, 3):
                pass
        #         newFrame[row][col][i] = f0[yi1][xi1][i]
        # dest[row][col] = f0[yi0][xi0]
    # for i in range(0,3):
    #     newFrame[row, col, i] = 255-newFrame[row,col,i]

    # newFrame[row,col,0]=red
    newFrame[row, col, 1] = green
    newFrame[row, col, 2] = blue

    # print(mask)
    # exit(0)
    # for row in tqdm(range(h), nested=True, desc="color transfer"):
    # for row in range(startX, w, gridX):
    #     for col in range(startY, h, gridY):
    #         maskedTransfer(f0, f1, newFrame, interp, mask,
    #                               row, col, t)


def transfer(frame0, frame1, forward, backward, interp, t=0.5):
    newFrame = np.copy(frame0)
    mask = getOcclusions(forward, backward, interp, t)

    d_newFrame = cuda.to_device(newFrame)
    # d_mask=cuda.to_device(mask)
    d_f0 = cuda.to_device(frame0)
    d_f1 = cuda.to_device(frame1)
    # d_forward=cuda.to_device(forward)
    # d_backward=cuda.to_device(backward)
    # d_interp=cuda.to_device(interp)
    print("started color transfer")

    threadsperblock = (16, 16)
    blockspergrid_x = math.ceil(newFrame.shape[0] / threadsperblock[0])
    blockspergrid_y = math.ceil(newFrame.shape[1] / threadsperblock[1])
    blockspergrid = (blockspergrid_x, blockspergrid_y)

    # d_newFrame=d_newFrame.astype(np.uint8, order='A')
    # d_mask=d_mask.astype(np.uint8, order='A')

    print(newFrame.shape)
    print(blockspergrid, threadsperblock)

    print("newType:", numba.typeof(newFrame))
    print("newType:", numba.typeof(d_newFrame))
    print("forward:", numba.typeof(forward))
    print("f0:", numba.typeof(frame0))
    print("d_f0:", numba.typeof(d_f0))
    print("f1:", numba.typeof(frame1))
    print("interp:", numba.typeof(interp))
    print("mask:", numba.typeof(mask))
    print("t:", numba.typeof(t))

    # colorTransGPU[blockspergrid,threadsperblock](f0, f1, forward, backward, interp, mask, d_newFrame, t)
    colorTransGPU[blockspergrid, threadsperblock](d_f0, d_f1, forward, forward, interp, mask, d_newFrame, t)
    # colorTransGPU[griddim,blockdim](d_f0, d_f1, d_forward, d_backward, d_interp, d_mask, d_newFrame, t)

    d_newFrame.to_host()

    print("finished color transfer")

    return newFrame


def getOcclusions(forward, backward, interp, t=0.5):
    h = interp.shape[0]
    w = interp.shape[1]
    mask = np.zeros((h, w, 2), dtype='uint8')
    # for row in tqdm(range(h), nested=True, desc="generating occlusion mask"):
    for row in tqdm(range(h), desc="generating occlusion mask"):
        for col in range(w):
            if backward is None:
                mask[row][col][0] = 1
                # mask[row][col][1] = 1
            else:
                getOcclusion(forward, backward, interp, mask,
                             row, col, t)
    return mask


def getOcclusion(forward, backward, interp, mask, row, col, t=0.5):
    h = interp.shape[0]
    w = interp.shape[1]
    im = interp[row][col]
    ux = im[0] / w
    uy = -im[1] / h
    (x, y) = manipulate.toCoord(interp, col, row)
    xp0 = x - t * ux
    yp0 = y - t * uy
    xp1 = x + t * ux
    yp1 = y + t * uy
    (xi0, yi0) = manipulate.toPixel(forward, xp0, yp0)
    (xi1, yi1) = manipulate.toPixel(backward, xp1, yp1)
    if (manipulate.inside(forward, xi0, yi0) and
            manipulate.inside(backward, xi1, yi1)):

        fvec = forward[yi0][xi0]
        # if backward is None:
        #     bvec=fvec
        # else:
        bvec = -1 * backward[yi1][xi1]

        d0 = manipulate.flowDist(fvec, im)
        d1 = manipulate.flowDist(bvec, im)
        diff = abs(d0 - d1)

        if d0 <= d1:
            mask[row][col][0] = 1
            if diff < 0.1:
                mask[row][col][1] = 1
        else:
            mask[row][col][1] = 1
            if diff < 0.1:
                mask[row][col][0] = 1
    else:
        mask[row][col][0] = 1

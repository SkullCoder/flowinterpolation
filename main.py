
import click
import cv2
import glob
import os

import interpolation.interpolate as inter
import interpolation.utility as utils


def createDirs():
    for dir in ["./frames", "./forward", "./backward", "./flows", "./flows/imgs"]:
        if not os.path.exists(dir):
            os.makedirs(dir)


def clean():
    for dir in ["frames", "backward", "forward"]:
        files = glob.glob(dir + "/*.ppm")
        for f in files:
            os.remove(f)
        files = glob.glob(dir + "/*.flo")
        for f in files:
            os.remove(f)


@click.command()
@click.option("-i", type=click.Path(exists=True),
              default="./in.avi",
              help="Input video path")
@click.option("-o", type=click.Path(),
              default="./out",
              help="Output video path")
@click.option("--calc-flows/--no-calc-flows",
              default=True,
              help="Recalculate optical flows")
@click.option("--ldof-path", type=click.Path(exists=True),
              default="./pami/ldof",
              help="ldof optical flow calculator executable path")
@click.option("--clean/--no-clean", default=False,
              help="Clean directories before running")
@click.option("--nframes", default=0, type=click.IntRange(0),
              help="Number of frames to process. 0 will process whole video")
@click.option("--processes", default=4, type=click.IntRange(0),
              help="Number of processes used.")
@click.option("--bidirectional/--forward-only",
              default=True,
              help="Use motion in both directions")
def main(i, o, calc_flows, ldof_path, clean, nframes, processes, bidirectional):
    createDirs()
    if clean:
        clean()
    frames = utils.load(i)
    if nframes != 0:
        frames = frames[0:nframes]
    utils.saveFramesImg(frames, "./frames")

    frame_count = len(frames)

    if calc_flows:
        utils.calcFlows(frame_count, "./frames", "./forward",
                        ldof_path, forward=True, processes=processes)
        if bidirectional:
            utils.calcFlows(frame_count, "./frames", "./backward",
                            ldof_path, forward=False, processes=processes)

    forward = utils.loadFlow(frame_count, "./forward", forward=True)
    if bidirectional:
        backward = utils.loadFlow(frame_count, "./backward", forward=False)
    else:
        backward = [None] * len(forward)

    (interleaved, flows) = inter.interpolateFrames(frames,
                                                   forward,
                                                   backward, processes=processes)

    i = 0
    for frame in interleaved:
        print("Int", i)
        cv2.imwrite("./outframes/" + str(i) + '.png', frame)
        i = i + 1

    # ut.view_frame_by_frame(interleaved)
    # ut.view_frame_by_frame(flows)
    utils.save(interleaved, o + ".avi")
    utils.saveFramesNpy(flows, o + ".npy")
    return


if __name__ == '__main__':
    main()
